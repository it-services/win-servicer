# Win-Servicer

Win Servicer was created to start a specified service at a specified interval.

## Getting started

You must specify the display name (DN) of the service in the script file (i.e. "Symplectic Elements Scheduler (Default)").  Running the script will check if the service is running, and will start it if it is stopped.  A log file is created in the root of %SYSTEMDRIVE%, named with the DN of the service.

Create a scheduled task to check the service is running at an interval of your choice.  The executable name to use is powershell.exe with arguments -FILE win-servicer.ps1.
