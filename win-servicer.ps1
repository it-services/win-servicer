﻿# win-servicer was created to start a Windows service at a specified interval

## PLEASE SET THE DISPLAY NAME OF THE WINDOWS SERVICE BELOW ##
$ServiceDN = "Symplectic Elements Scheduler (Default)"


# Get the current date/time
$ThisDate = Get-Date

# Set log file
$LogFile = Join-Path $env:SystemDrive ($ServiceDN + ".log")

# Logging mechanism
Function Write-ThisServiceLog
{
   Param ([string]$logstring)

   Add-Content $Logfile -value $logstring
}

# Get the service object
$ServiceOBJ = Get-Service -DisplayName $ServiceDN -ErrorAction SilentlyContinue

If ($ServiceOBJ.Status -eq "Stopped") {
    try {
        Start-Service $serviceDN
        Write-ThisServiceLog $ThisDate
        Write-ThisServiceLog "$ServiceDN has been started"
    }
    catch [Exception] {
        Write-ThisServiceLog $ThisDate
        Write-ThisServiceLog $_.Exception.Message;
        break
    }
}
ElseIf ($ServiceOBJ.Status -eq "Running") {
    Write-ThisServiceLog $ThisDate
    Write-ThisServiceLog "$ServiceDN is already running."
}
Else {
    Write-ThisServiceLog $ThisDate
    Write-ThisServiceLog "$ServiceDN could not be found."
}

    
     